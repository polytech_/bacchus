﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class ModifyMarqueForm : Form
    {
        /// <summary>
        /// La référence de la marque
        /// </summary>
        private int RefMarque;

        /// <summary>
        /// La nom de la marque
        /// </summary>
        private string NomMarque;

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public ModifyMarqueForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialise les données de la marque à modifier
        /// </summary>
        /// <param name="Marque">La marque à modifier</param>
        public void InitData(Marque Marque)
        {
            RefMarque = Marque.RefMarque;
            NomMarque = Marque.Nom;

            RefMarqueTextBox.Text = Marque.RefMarque.ToString();
            NomMarqueTextBox.Text = Marque.Nom;
        }


        /// <summary>
        /// Event du click sur le bouton pour confirmer la modification de la marque
        /// Vérifie que les champs ne sont pas vides
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            // On remarque d'abord si le nom de la marque est non vide
            if (NomMarqueTextBox.Text.Equals(""))
            {
                MessageBox.Show(@"Vous n'avez pas saisi de nom pour la marque", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                var MarqueExistente = ((FormMain)Owner).BacchusData.GetMarque(NomMarqueTextBox.Text);

                // On regarde ensuite si il n'existe pas déjà une marque avec le nom de la marque modifié, qui ne soit pas la marque actuelle
                if (MarqueExistente != null && !MarqueExistente.Nom.Equals(NomMarque))
                {
                    MessageBox.Show(@"Une marque avec ce nom existe déjà, merci d'en rentrer un qui n'existe pas dans la base de donnée", @"Attention", MessageBoxButtons.OK);
                }
                else
                {
                    // On modifie la marque si toutes les conditions sont validées
                    var Marque = ((FormMain)Owner).BacchusData.GetMarque(RefMarque);
                    Marque.Nom = NomMarqueTextBox.Text;

                    DAO.Modify(Marque);

                    ((FormMain)Owner).UpdateListView();
                    Close();
                }
            }

        }

        /// <summary>
        /// Event de la fermeture de la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
