﻿namespace Bacchus
{
    partial class FormImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EcrModeBtn = new System.Windows.Forms.Button();
            this.AjModeBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.FilePathInput = new System.Windows.Forms.TextBox();
            this.ChooseFileBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.ProgressBar = new System.Windows.Forms.ProgressBar();
            this.progressWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // EcrModeBtn
            // 
            this.EcrModeBtn.Location = new System.Drawing.Point(175, 220);
            this.EcrModeBtn.Name = "EcrModeBtn";
            this.EcrModeBtn.Size = new System.Drawing.Size(124, 29);
            this.EcrModeBtn.TabIndex = 1;
            this.EcrModeBtn.Text = "Mode écrasement";
            this.EcrModeBtn.UseVisualStyleBackColor = true;
            this.EcrModeBtn.Click += new System.EventHandler(this.EcrModeBtn_Click);
            // 
            // AjModeBtn
            // 
            this.AjModeBtn.Location = new System.Drawing.Point(305, 220);
            this.AjModeBtn.Name = "AjModeBtn";
            this.AjModeBtn.Size = new System.Drawing.Size(87, 29);
            this.AjModeBtn.TabIndex = 2;
            this.AjModeBtn.Text = "Mode ajout";
            this.AjModeBtn.UseVisualStyleBackColor = true;
            this.AjModeBtn.Click += new System.EventHandler(this.AjModeBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Fichier séléctionné";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Progression :";
            // 
            // FilePathInput
            // 
            this.FilePathInput.Enabled = false;
            this.FilePathInput.Location = new System.Drawing.Point(16, 77);
            this.FilePathInput.Name = "FilePathInput";
            this.FilePathInput.Size = new System.Drawing.Size(500, 22);
            this.FilePathInput.TabIndex = 7;
            // 
            // ChooseFileBtn
            // 
            this.ChooseFileBtn.Location = new System.Drawing.Point(523, 76);
            this.ChooseFileBtn.Name = "ChooseFileBtn";
            this.ChooseFileBtn.Size = new System.Drawing.Size(29, 26);
            this.ChooseFileBtn.TabIndex = 8;
            this.ChooseFileBtn.Text = "...";
            this.ChooseFileBtn.UseVisualStyleBackColor = true;
            this.ChooseFileBtn.Click += new System.EventHandler(this.ChooseFileBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(132, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(309, 19);
            this.label2.TabIndex = 9;
            this.label2.Text = "Sélectionner un fichier CSV à importer :";
            // 
            // ProgressBar
            // 
            this.ProgressBar.Location = new System.Drawing.Point(16, 153);
            this.ProgressBar.Name = "ProgressBar";
            this.ProgressBar.Size = new System.Drawing.Size(537, 23);
            this.ProgressBar.TabIndex = 6;
            // 
            // FormImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(565, 261);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ChooseFileBtn);
            this.Controls.Add(this.FilePathInput);
            this.Controls.Add(this.ProgressBar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.AjModeBtn);
            this.Controls.Add(this.EcrModeBtn);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MaximumSize = new System.Drawing.Size(581, 300);
            this.MinimumSize = new System.Drawing.Size(581, 300);
            this.Name = "FormImport";
            this.Text = "Import d\'fichier CSV";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button EcrModeBtn;
        private System.Windows.Forms.Button AjModeBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox FilePathInput;
        private System.Windows.Forms.Button ChooseFileBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar ProgressBar;
        private System.ComponentModel.BackgroundWorker progressWorker;
    }
}