﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class ModifyArticleForm : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public ModifyArticleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialise les données de l'article à modifier
        /// </summary>
        /// <param name="Article">L'article à modifier</param>
        public void InitData(Article Article)
        {
            // On remplit les champs texte
            RefTextBox.Text = Article.RefArticle;
            PrixTextBox.Text = Article.PrixHT.ToString(CultureInfo.CurrentCulture);
            QteTextBox.Text = Article.Quantite.ToString();
            DescriptionRTextBox.Text = Article.DescriptionArticle;

            var BacchusData = ((FormMain)Owner).BacchusData;

            // On remplit la comboBox des familles
            FamilleComboBox.DataSource = BacchusData.FamilleList;
            FamilleComboBox.DisplayMember = "Nom";
            FamilleComboBox.ValueMember = "RefFamille";
            FamilleComboBox.SelectedItem = Article.SousFamille.Famille;
            FamilleComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            // On remplit la comboBox des marques
            MarqueComboBox.DataSource = BacchusData.MarqueList;
            MarqueComboBox.DisplayMember = "Nom";
            MarqueComboBox.ValueMember = "RefMarque";
            MarqueComboBox.SelectedItem = Article.Marque;
            MarqueComboBox.DropDownStyle = ComboBoxStyle.DropDownList;

            /*
             * La comboBox des sous familles se charge par un évenement "indexChanged" de la box des familles
             * On set donc juste le bon nom de sous familles
             */
            SousFamilleComboBox.DataSource = DAO.GetSousFamillesByFamille(Article.SousFamille.Famille);
            SousFamilleComboBox.DisplayMember = "Nom";
            SousFamilleComboBox.ValueMember = "RefSousFamille";
            SousFamilleComboBox.SelectedText = Article.SousFamille.Nom;
            SousFamilleComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        /// <summary>
        /// Event du click sur le bouton pour confirmer la modification de l'article
        /// Vérifie que les champs ne sont pas vides
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            var BacchusData = ((FormMain)Owner).BacchusData;

            if (QteTextBox.Text.Equals("") || PrixTextBox.Text.Equals("") || DescriptionRTextBox.Text.Equals(""))
            {
                MessageBox.Show(@"Merci d'insérer des valeurs pour la quantité, le prix, et/ou la description de l'article !",
                  @"Attention",
                    MessageBoxButtons.OK);
            }
            else
            {
                var Article = BacchusData.GetArticle(RefTextBox.Text);

                Article.DescriptionArticle = DescriptionRTextBox.Text;
                Article.Quantite = int.Parse(QteTextBox.Text);
                Article.PrixHT = float.Parse(PrixTextBox.Text, CultureInfo.InvariantCulture);

                Article.SousFamille = BacchusData.GetSousFamille(SousFamilleComboBox.Text);
                Article.Marque = BacchusData.GetMarque(MarqueComboBox.Text);

                DAO.Modify(Article);

                ((FormMain)Owner).UpdateListView();
                Close();
            }
        }

        /// <summary>
        /// Event du click sur le bouton pour annuler la modification
        /// Ferme la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
