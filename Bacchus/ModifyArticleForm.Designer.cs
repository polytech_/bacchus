﻿namespace Bacchus
{
    partial class ModifyArticleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RefTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MarqueComboBox = new System.Windows.Forms.ComboBox();
            this.FamilleComboBox = new System.Windows.Forms.ComboBox();
            this.SousFamilleComboBox = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.PrixTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.DescriptionRTextBox = new System.Windows.Forms.RichTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.QteTextBox = new System.Windows.Forms.TextBox();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.AbortButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RefTextBox
            // 
            this.RefTextBox.Enabled = false;
            this.RefTextBox.Location = new System.Drawing.Point(173, 53);
            this.RefTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RefTextBox.Name = "RefTextBox";
            this.RefTextBox.Size = new System.Drawing.Size(175, 22);
            this.RefTextBox.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Référence :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 220);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sous famille :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Marque :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(72, 165);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "Famille :";
            // 
            // MarqueComboBox
            // 
            this.MarqueComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MarqueComboBox.FormattingEnabled = true;
            this.MarqueComboBox.Location = new System.Drawing.Point(173, 110);
            this.MarqueComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarqueComboBox.Name = "MarqueComboBox";
            this.MarqueComboBox.Size = new System.Drawing.Size(175, 24);
            this.MarqueComboBox.TabIndex = 5;
            // 
            // FamilleComboBox
            // 
            this.FamilleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FamilleComboBox.FormattingEnabled = true;
            this.FamilleComboBox.Location = new System.Drawing.Point(173, 161);
            this.FamilleComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.FamilleComboBox.Name = "FamilleComboBox";
            this.FamilleComboBox.Size = new System.Drawing.Size(175, 24);
            this.FamilleComboBox.TabIndex = 6;
            // 
            // SousFamilleComboBox
            // 
            this.SousFamilleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SousFamilleComboBox.FormattingEnabled = true;
            this.SousFamilleComboBox.Location = new System.Drawing.Point(173, 217);
            this.SousFamilleComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SousFamilleComboBox.Name = "SousFamilleComboBox";
            this.SousFamilleComboBox.Size = new System.Drawing.Size(175, 24);
            this.SousFamilleComboBox.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(72, 279);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Prix H.T :";
            // 
            // PrixTextBox
            // 
            this.PrixTextBox.Location = new System.Drawing.Point(173, 276);
            this.PrixTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PrixTextBox.Name = "PrixTextBox";
            this.PrixTextBox.Size = new System.Drawing.Size(175, 22);
            this.PrixTextBox.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(72, 374);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 16);
            this.label6.TabIndex = 11;
            this.label6.Text = "Description :";
            // 
            // DescriptionRTextBox
            // 
            this.DescriptionRTextBox.Location = new System.Drawing.Point(173, 337);
            this.DescriptionRTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DescriptionRTextBox.Name = "DescriptionRTextBox";
            this.DescriptionRTextBox.Size = new System.Drawing.Size(175, 85);
            this.DescriptionRTextBox.TabIndex = 12;
            this.DescriptionRTextBox.Text = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(72, 460);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Quantité :";
            // 
            // QteTextBox
            // 
            this.QteTextBox.Location = new System.Drawing.Point(173, 457);
            this.QteTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.QteTextBox.Name = "QteTextBox";
            this.QteTextBox.Size = new System.Drawing.Size(175, 22);
            this.QteTextBox.TabIndex = 13;
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(132, 526);
            this.ConfirmButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(87, 28);
            this.ConfirmButton.TabIndex = 15;
            this.ConfirmButton.Text = "Confirmer";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(226, 526);
            this.AbortButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(87, 28);
            this.AbortButton.TabIndex = 16;
            this.AbortButton.Text = "Annuler";
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.AbortButton_Click);
            // 
            // ModifyArticleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 567);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.QteTextBox);
            this.Controls.Add(this.DescriptionRTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PrixTextBox);
            this.Controls.Add(this.SousFamilleComboBox);
            this.Controls.Add(this.FamilleComboBox);
            this.Controls.Add(this.MarqueComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RefTextBox);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(441, 606);
            this.MinimumSize = new System.Drawing.Size(441, 606);
            this.Name = "ModifyArticleForm";
            this.Text = "Modifier un article";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox RefTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox MarqueComboBox;
        private System.Windows.Forms.ComboBox FamilleComboBox;
        private System.Windows.Forms.ComboBox SousFamilleComboBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PrixTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox DescriptionRTextBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox QteTextBox;
        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.Button AbortButton;
    }
}