﻿using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;


namespace DAOService
{
    class DAO
    {
        /// <summary>
        /// Attribut nous permettant de nous connecter à la base de données
        /// </summary>
        private static readonly SQLiteConnection DatabaseConnection = new SQLiteConnection("Data Source=Bacchus.SQLite");

        /// <summary>
        /// Fonction initialisant la connexion avec la base de données
        /// </summary>
        private static void InitializeConnection()
        {
            if (DatabaseConnection.State == ConnectionState.Closed)
                DatabaseConnection.Open();
        }


        /// <summary>
        /// Fonction mettant fin à la connexion avec la base de données
        /// </summary>
        private static void CloseConnexion()
        {
            if (DatabaseConnection.State == ConnectionState.Open)
                DatabaseConnection.Close();
        }



        /// <summary>
        /// Fonction récupérant tout les articles de la base de données
        /// </summary>
        /// <param name="BacchusData">Le modèle de stockage</param>
        /// <returns>La liste de tout les articles</returns>
        public static List<Article> GetAllArticles(BacchusData BacchusData)
        {
            var Articles = new List<Article>();

            InitializeConnection();
            var Command = new SQLiteCommand("SELECT * FROM Articles", DatabaseConnection);
            var Reader = Command.ExecuteReader();

            while (Reader.Read())
            {
                var RefArticle = Reader.GetString(0);
                var Description = Reader.GetString(1);
                var SousFamille = BacchusData.GetSousFamille(Reader.GetInt32(2));
                var Marque = BacchusData.GetMarque(Reader.GetInt32(3));
                var PrixHt = Reader.GetFloat(4);
                var Quantite = Reader.GetInt32(5);

                Articles.Add(new Article(RefArticle, Description, SousFamille, Marque, PrixHt, Quantite));
            }

            Reader.Close();
            CloseConnexion();

            return Articles;
        }


        /// <summary>
        /// Fonction récupérant l'article en fonction de la réf passée en paramètre
        /// </summary>
        /// <param name="PRefArticle">La réf de l'article</param>
        /// <returns>L'article</returns>
        public static Article GetArticle(string PRefArticle)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM Articles WHERE RefArticle = :refArticle ", DatabaseConnection);
            Command.Parameters.AddWithValue("refArticle", PRefArticle);
            var Reader = Command.ExecuteReader();

            Article Article = new Article();
            while (Reader.Read())
            {
                var Ref = Reader.GetString(0);
                var Description = Reader.GetString(1);
                var RefSousFamille = Reader.GetInt32(2);
                var RefMarque = Reader.GetInt32(3);
                var PrixHT = Reader.GetFloat(4);
                var Quantite = Reader.GetInt32(5);

                Article.RefArticle = Ref;
                Article.DescriptionArticle = Description;
                Article.SousFamille = GetSousFamilleByReference(RefSousFamille);
                Article.Marque = GetMarqueByReference(RefMarque);
                Article.PrixHT = PrixHT;
                Article.Quantite = Quantite;
            }

            Reader.Close();
            CloseConnexion();

            return Article;
        }

        /// <summary>
        /// Fonction ajoutant un article dans la base de donnée
        /// </summary>
        /// <param name="PArticle">L'article à ajouter</param>
        public static Article AddArticle(Article PArticle)
        {
            InitializeConnection();
            
            using (var Command = new SQLiteCommand(DatabaseConnection))
            {
              
                using (var Transaction = DatabaseConnection.BeginTransaction())
                {
                    Command.CommandText =
                        "INSERT INTO Articles (RefArticle, Description, RefSousFamille, RefMarque, PrixHT, Quantite) VALUES (:refArticle, :description, :refSousFamille, :refMarque, :prixHT, :quantite)";

                    Command.Parameters.AddWithValue("refArticle", PArticle.RefArticle);
                    Command.Parameters.AddWithValue("description", PArticle.DescriptionArticle);
                    Command.Parameters.AddWithValue("refSousFamille", PArticle.SousFamille.RefSousFamille);
                    Command.Parameters.AddWithValue("refMarque", PArticle.Marque.RefMarque);
                    Command.Parameters.AddWithValue("prixHT", PArticle.PrixHT);
                    Command.Parameters.AddWithValue("quantite", PArticle.Quantite);

                    Command.ExecuteNonQuery();

                    Transaction.Commit();
                    Command.Dispose();
                }

            }

            CloseConnexion();
            return PArticle;
        }

        /// <summary>
        /// Fonction supprimant un article de la base de donnée
        /// </summary>
        /// <param name="Article">L'article à supprimer</param>
        public static void Delete(Article Article)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("DELETE FROM Articles WHERE RefArticle = :refArticle", DatabaseConnection);
            Command.Parameters.AddWithValue("refArticle", Article.RefArticle);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }

        /// <summary>
        /// Fonction modifiant un article de la base de donnée
        /// </summary>
        /// <param name="Article">L'article à modifier</param>
        public static void Modify(Article Article)
        {
            InitializeConnection();

            var Command =
                new SQLiteCommand(
                    "UPDATE Articles SET Description = :descript, RefSousFamille = :refSousFamille, RefMarque = :refMarque, PrixHT = :prix, Quantite = :quantite WHERE RefArticle = :refArticle",
                    DatabaseConnection);
            Command.Parameters.AddWithValue("descript", Article.DescriptionArticle);
            Command.Parameters.AddWithValue("refSousFamille", Article.SousFamille.RefSousFamille);
            Command.Parameters.AddWithValue("refMarque", Article.Marque.RefMarque);
            Command.Parameters.AddWithValue("prix", Article.PrixHT);
            Command.Parameters.AddWithValue("quantite", Article.Quantite);
            Command.Parameters.AddWithValue("refArticle", Article.RefArticle);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }



        /// <summary>
        /// Fonction récupérant tout les marques de la base de données
        /// </summary>
        /// <returns>La liste de tout les marques</returns>
        private static List<Marque> GetAllMarques()
        {
            var Marques = new List<Marque>();

            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM Marques", DatabaseConnection);
            var Reader = Command.ExecuteReader();

            while (Reader.Read())
            {
                var RefMarque = Reader.GetInt32(0);
                var Nom = Reader.GetString(1);
                Marques.Add(new Marque(RefMarque, Nom));
            }

            Reader.Close();
            CloseConnexion();

            return Marques;
        }

        /// <summary>
        /// Fonction récupérant la marque en fonction de la réf passée en paramètre
        /// </summary>
        /// <param name="PRefMarque">La réf de la marque</param>
        /// <returns>La marque</returns>
        public static Marque GetMarqueByReference(int PRefMarque)
        { 
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM Marques WHERE RefMarque = :refMarque ", DatabaseConnection);
            Command.Parameters.AddWithValue("refMarque", PRefMarque);
            var Reader = Command.ExecuteReader();

            Marque Marque = new Marque();
            while (Reader.Read())
            {
                var Ref = Reader.GetInt32(0);
                var Nom = Reader.GetString(1);

                Marque.RefMarque = Ref;
                Marque.Nom = Nom;  
            }

            Reader.Close();
            CloseConnexion();

            return Marque;
        }

        /// <summary>
        /// Fonction récupérant la marque en fonction du nom de marque passé en paramètre
        /// </summary>
        /// <param name="PNomMarque">Le nom de la marque</param>
        /// <returns>La marque</returns>
        public static Marque GetMarqueByName(string PNomMarque)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM Marques WHERE Nom = :NomMarque ", DatabaseConnection);
            Command.Parameters.AddWithValue("NomMarque", PNomMarque);
            var Reader = Command.ExecuteReader();

            Marque marque = new Marque();
            while (Reader.Read())
            {
                var Ref = Reader.GetInt32(0);
                var Nom = Reader.GetString(1);

                marque.RefMarque = Ref;
                marque.Nom = Nom;
            }

            Reader.Close();
            CloseConnexion();

            return marque;
        }


        /// <summary>
        /// Fonction ajoutant une marque dans la base de donnée
        /// </summary>
        /// <param name="PMarque">La marque à ajouter</param>
        /// <returns>La marque </returns>
        public static Marque AddMarque(Marque PMarque)
        {
            InitializeConnection();
            
            using (var Command = new SQLiteCommand(DatabaseConnection))
            {
                Command.CommandText = "SELECT MAX(RefMarque) FROM Marques";
                var Scalar = Command.ExecuteScalar();
                if (Scalar.ToString().Equals(""))
                {
                    PMarque.RefMarque = 0;
                }
                else
                {
                    PMarque.RefMarque = Convert.ToInt32(Scalar) + 1;
                }
                
                using (var Transaction = DatabaseConnection.BeginTransaction())
                {
                    Command.CommandText =
                        "INSERT INTO Marques (RefMarque, Nom) VALUES (:refMarque, :nom)";

                    Command.Parameters.AddWithValue("refMarque", PMarque.RefMarque);
                    Command.Parameters.AddWithValue("nom", PMarque.Nom);
                 
                    Command.ExecuteNonQuery();

                    Transaction.Commit();
                    Command.Dispose();
                }

            }

            CloseConnexion();
            return PMarque;
        }

        /// <summary>
        /// Fonction supprimant une marque de la base de donnée
        /// </summary>
        /// <param name="Marque">La marque à supprimer</param>
        public static void Delete(Marque Marque)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("DELETE FROM Marques WHERE RefMarque = :refMarque", DatabaseConnection);
            Command.Parameters.AddWithValue("refMarque", Marque.RefMarque);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }

        /// <summary>
        /// Fonction modifiant une marque de la base de donnée
        /// </summary>
        /// <param name="Marque">La marque à modifier</param>
        public static void Modify(Marque Marque)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("UPDATE Marques SET Nom = :nom WHERE RefMarque = :refMarque", DatabaseConnection);
            Command.Parameters.AddWithValue("nom", Marque.Nom);
            Command.Parameters.AddWithValue("refMarque", Marque.RefMarque);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }


        /// <summary>
        /// Fonction récupérant toutes les sous familles de la base de données
        /// </summary>
        /// <param name="BacchusData">Le modèle de stockage</param>
        /// <returns>La liste de toutes les sous familles </returns>
        private static List<SousFamille> GetAllSousFamilles(BacchusData BacchusData)
        {
            var SousFamilles = new List<SousFamille>();

            InitializeConnection();
            var Command = new SQLiteCommand("SELECT * FROM SousFamilles", DatabaseConnection);
            var Reader = Command.ExecuteReader();

            while (Reader.Read())
            {
                var RefSousFamille = Reader.GetInt32(0);
                var Famille = BacchusData.GetFamille(Reader.GetInt32(1));
                var Nom = Reader.GetString(2);

                SousFamilles.Add(new SousFamille(RefSousFamille, Famille, Nom));
            }

            Reader.Close();
            CloseConnexion();

            return SousFamilles;
        }


        /// <summary>
        /// Fonction récupérant la sous famille en fonction de sa référence
        /// </summary>
        /// <param name="RefSousFamille">La référence de la sous famille</param>
        /// <returns>La sous famille </returns>
        public static SousFamille GetSousFamilleByReference(int RefSousFamille)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM SousFamilles WHERE RefSousFamille = :refSF ", DatabaseConnection);
            Command.Parameters.AddWithValue("refSF", RefSousFamille);
            var Reader = Command.ExecuteReader();

            SousFamille SFam = new SousFamille();
            while (Reader.Read())
            {
                var Ref = Reader.GetInt32(0);
                var RefFamille = Reader.GetInt32(1);
                var Nom = Reader.GetString(2);

                SFam.RefSousFamille = Ref;
                SFam.Famille = GetFamilleByReference(RefFamille); 
                SFam.Nom = Nom;
            }

            Reader.Close();
            CloseConnexion();

            return SFam;
        }

        /// <summary>
        /// Fonction récupérant la sous famille en fonction de son nom
        /// </summary>
        /// <param name="PNom">Le nom de la sous famille</param>
        /// <returns>La sous famille </returns>
        public static SousFamille GetSousFamilleByName(string PNom)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM SousFamilles WHERE Nom = :pNom ", DatabaseConnection);
            Command.Parameters.AddWithValue("pNom", PNom);
            var Reader = Command.ExecuteReader();

            SousFamille SFam = new SousFamille();
            while (Reader.Read())
            {
                var Ref = Reader.GetInt32(0);
                var RefFamille = Reader.GetInt32(1);
                var Nom = Reader.GetString(2);

                SFam.RefSousFamille = Ref;
                SFam.Famille = GetFamilleByReference(RefFamille);
                SFam.Nom = Nom;
            }

            Reader.Close();
            CloseConnexion();

            return SFam;
        }

        /// <summary>
        /// Fonction ajoutant la sous famille passée en paramètre
        /// </summary>
        /// <param name="PSousFamille">la sous famille</param>
        /// <returns>La sous famille </returns>
        public static SousFamille AddSousFamille(SousFamille PSousFamille)
        {
            InitializeConnection();
           

            using (var Command = new SQLiteCommand(DatabaseConnection))
            {
                Command.CommandText = "SELECT MAX(RefSousFamille) FROM SousFamilles";
                var Scalar = Command.ExecuteScalar();
                if (Scalar.ToString().Equals(""))
                {
                    PSousFamille.RefSousFamille = 0;
                }
                else
                {
                    PSousFamille.RefSousFamille = Convert.ToInt32(Scalar) + 1;
                }

                using (var Transaction = DatabaseConnection.BeginTransaction()
)
                {
                    Command.CommandText =
                        "INSERT INTO SousFamilles (RefSousFamille, RefFamille, Nom) VALUES (:refSousFamille, :refFamille, :nom)";

                    Command.Parameters.AddWithValue("refSousFamille", PSousFamille.RefSousFamille);                    
                    Command.Parameters.AddWithValue("refFamille", PSousFamille.Famille.RefFamille);
                    Command.Parameters.AddWithValue("nom", PSousFamille.Nom);

                    Command.ExecuteNonQuery();

                    Transaction.Commit();
                    Command.Dispose();
                }

            }

            CloseConnexion();
            return PSousFamille;

        }

        /// <summary>
        /// Fonction supprimant la sous famille passée en paramètre
        /// </summary>
        /// <param name="SousFamille">la sous famille</param>
        public static void Delete(SousFamille SousFamille)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("DELETE FROM SousFamilles WHERE RefSousFamille = :refSousFamille", DatabaseConnection);
            Command.Parameters.AddWithValue("refSousFamille", SousFamille.RefSousFamille);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }


        /// <summary>
        /// Fonction récupérant toutes les sous familles de la base de données 
        /// en fonction de la famille passée en paramètre
        /// </summary>
        /// <param name="Famille">Le famille</param>
        /// <returns>La liste de toutes les sous familles appartenant à la famille</returns>
        public static List<SousFamille> GetSousFamillesByFamille(Famille Famille)
        {
            var SousFamilles = new List<SousFamille>();

            InitializeConnection();
            var Command = new SQLiteCommand("SELECT * FROM SousFamilles WHERE RefFamille = :refFamille", DatabaseConnection);
            Command.Parameters.AddWithValue("refFamille", Famille.RefFamille);
            var Reader = Command.ExecuteReader();

            while (Reader.Read())
            {
                var RefSousFamille = Reader.GetInt32(0);
                var Nom = Reader.GetString(2);

                SousFamilles.Add(new SousFamille(RefSousFamille, Famille, Nom));
            }

            Reader.Close();
            CloseConnexion();

            return SousFamilles;
        }

        /// <summary>
        /// Fonction modifiant la sous famille passée en paramètre
        /// </summary>
        /// <param name="SousFamille">la sous famille</param>
        public static void Modify(SousFamille SousFamille)
        {
            InitializeConnection();

            var Command =
                new SQLiteCommand(
                    "UPDATE SousFamilles SET Nom = :nom, RefFamille = :refFamille WHERE RefSousFamille = :refSousFamille",
                    DatabaseConnection);

            Command.Parameters.AddWithValue("nom", SousFamille.Nom);
            Command.Parameters.AddWithValue("refFamille", SousFamille.Famille.RefFamille);
            Command.Parameters.AddWithValue("refSousFamille", SousFamille.RefSousFamille);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }





        /// <summary>
        /// Fonction récupérant toutes les familles de la base de données 
        /// </summary>
        /// <returns>La liste de toutes les familles</returns>
        private static List<Famille> GetAllFamilles()
        {
            var Familles = new List<Famille>();

            InitializeConnection();
            var Command = new SQLiteCommand("SELECT * FROM Familles", DatabaseConnection);
            var Reader = Command.ExecuteReader();

            while (Reader.Read())
            {
                var RefFamille = Reader.GetInt32(0);
                var Nom = Reader.GetString(1);
                Familles.Add(new Famille(RefFamille, Nom));
            }

            Reader.Close();
            CloseConnexion();

            return Familles;
        }

        /// <summary>
        /// Fonction récupérant une famille en fonction de sa référence
        /// </summary>
        /// <param name="RefFamille">Le référence de la famille</param>
        /// <returns>La famille</returns>
        public static Famille GetFamilleByReference(int RefFamille)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM Familles WHERE RefFamille = :refF", DatabaseConnection);
            Command.Parameters.AddWithValue("refF", RefFamille);
            var Reader = Command.ExecuteReader();

            Famille Fam = new Famille();
            while (Reader.Read())
            {
                var Ref = Reader.GetInt32(0);
                var Nom = Reader.GetString(1);

                Fam.RefFamille = Ref;
                Fam.Nom = Nom;
            }

            Reader.Close();
            CloseConnexion();

            return Fam;
        }

        /// <summary>
        /// Fonction récupérant une famille en fonction de son nom
        /// </summary>
        /// <param name="PNom">Le nom de la famille</param>
        /// <returns>La famille</returns>
        public static Famille GetFamilleByName(string PNom)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("SELECT * FROM Familles WHERE Nom = :pNom ", DatabaseConnection);
            Command.Parameters.AddWithValue("pNom", PNom);
            var Reader = Command.ExecuteReader();

            Famille Fam = new Famille();
            while (Reader.Read())
            {
                var Ref = Reader.GetInt32(0);
                var Nom = Reader.GetString(1);

                Fam.RefFamille = Ref;
                Fam.Nom = Nom;
            }

            Reader.Close();
            CloseConnexion();

            return Fam;
        }


        /// <summary>
        /// Fonction ajoutant une famille
        /// </summary>
        /// <param name="PFamille">La famille</param>
        /// <returns>La famille</returns>
        public static Famille AddFamille(Famille PFamille)
        {
            InitializeConnection();
            
            using (var Command = new SQLiteCommand(DatabaseConnection))
            {
                Command.CommandText = "SELECT MAX(RefFamille) FROM Familles";
                var Scalar = Command.ExecuteScalar();
                if (Scalar.ToString().Equals(""))
                {
                    PFamille.RefFamille = 0;
                }
                else
                {
                    PFamille.RefFamille = Convert.ToInt32(Scalar) + 1;
                }

                using (var Transaction = DatabaseConnection.BeginTransaction())
                {
                    Command.CommandText =
                        "INSERT INTO Familles (RefFamille, Nom) VALUES (:refFamille, :nom)";

                    Command.Parameters.AddWithValue("refFamille", PFamille.RefFamille);
                    Command.Parameters.AddWithValue("nom", PFamille.Nom);

                    Command.ExecuteNonQuery();

                    Transaction.Commit();
                    Command.Dispose();
                }

            }

            CloseConnexion();
            return PFamille;
        }

        /// <summary>
        /// Fonction supprimant une famille
        /// </summary>
        /// <param name="Famille">La famille à supprimer</param>
        public static void Delete(Famille Famille)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("DELETE FROM Familles WHERE RefFamille = :refFamille", DatabaseConnection);
            Command.Parameters.AddWithValue("refFamille", Famille.RefFamille);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }

        /// <summary>
        /// Fonction modifiant une famille
        /// </summary>
        /// <param name="Famille">La famille à modifier</param>
        public static void Modify(Famille Famille)
        {
            InitializeConnection();

            var Command = new SQLiteCommand("UPDATE Familles SET Nom = :nom WHERE RefFamille = :refFamille", DatabaseConnection);
            Command.Parameters.AddWithValue("nom", Famille.Nom);
            Command.Parameters.AddWithValue("refFamille", Famille.RefFamille);
            Command.ExecuteNonQuery();

            CloseConnexion();
        }


        /// <summary>
        /// Fonction vidant toutes les tables
        /// </summary>
        public static void ClearAllTables()
        {
            InitializeConnection();
            var Transaction = DatabaseConnection.BeginTransaction();

            var Command = new SQLiteCommand("DELETE FROM Articles", DatabaseConnection);
            Command.ExecuteNonQuery();
            Command.CommandText = "DELETE FROM Familles";
            Command.ExecuteNonQuery();

            Command.CommandText = "DELETE FROM Marques";
            Command.ExecuteNonQuery();

            Command.CommandText = "DELETE FROM SousFamilles";
            Command.ExecuteNonQuery();

            Transaction.Commit();
            CloseConnexion();
        }

        /// <summary>
        /// Fonction initialisant toutes les données
        /// </summary>
        /// <returns>La modèle de stockage</returns>
        public static BacchusData InitialiseAll()
        {
            var BacchusData = new BacchusData
            {
                MarqueList = GetAllMarques(),
                FamilleList = GetAllFamilles(),
            };
            BacchusData.SousFamilleList = GetAllSousFamilles(BacchusData);
            BacchusData.ArticleList = GetAllArticles(BacchusData);

            return BacchusData;
        }

    }
}
