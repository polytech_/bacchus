﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Famille
    {

        public int RefFamille { get; set; }

        public String Nom { get; set; }

        public Famille() { }

        /// <summary>
        /// Constructeurs avec la référence et le nom de la famille
        /// </summary>
        public Famille(int pRefFamille, String pNomFamille)
        {
            this.RefFamille = pRefFamille;
            this.Nom = pNomFamille;
        }

        /// <summary>
        /// Constructeurs avec le nom de la famille
        /// </summary>
        public Famille(String pNomFamille)
        {
            this.Nom = pNomFamille;
        }

        /// <summary>
        /// Fonction retournant un tableau de string de la famille
        /// </summary>
        /// <returns>Le tableau de caractère</returns>
        public string[] ToRow()
        {
            string[] Row = { Nom };
            return Row;
        }
    }
}
