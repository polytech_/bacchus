﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Marque
    {
        public int RefMarque { get; set; }

        public String Nom { get; set; }

        public Marque() { }

        /// <summary>
        /// Constructeurs avec le nom de la marque
        /// </summary>
        public Marque(String pNomMarque)
        {
            this.Nom = pNomMarque;
        }


        /// <summary>
        /// Constructeurs avec la référence et le nom de la marque
        /// </summary>
        public Marque(int prefMarque, String pNomMarque)
        {
            this.RefMarque = prefMarque;
            this.Nom = pNomMarque;
        }

        /// <summary>
        /// Fonction retournant un tableau de string de la marque
        /// </summary>
        /// <returns>Le tableau de caractère</returns>
        public string[] ToRow()
        {
            string[] Row = { Nom };
            return Row;
        }
    }
}
