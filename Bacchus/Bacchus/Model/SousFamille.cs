﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class SousFamille
    {
        public int RefSousFamille { get; set; }

        public Famille Famille { get; set; }

        public String Nom { get; set; }

        public SousFamille() { }


        /// <summary>
        /// Constructeurs avec la référence, la famille et le nom de la famille
        /// de la sous famille
        /// </summary>
        public SousFamille(int PRefSousFamille, Famille PFamille, String PNom)
        {
            this.Famille = PFamille;
            this.Nom = PNom;
            this.RefSousFamille = PRefSousFamille;
        }


        /// <summary>
        /// Constructeurs avec la famille et le nom de la sous famille
        /// </summary>
        public SousFamille(Famille PFamille, String PNom)
        {
            this.Famille = PFamille;
            this.Nom = PNom;
       
        }

        /// <summary>
        /// Fonction retournant un tableau de string de la sous famille
        /// </summary>
        /// <returns>Le tableau de caractère</returns>
        public string[] ToRow()
        {
            string[] Row = { Nom, Famille.Nom };
            return Row;
        }
    }
}
