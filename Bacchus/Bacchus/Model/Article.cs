﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Model
{
    public class Article
    {
        public string RefArticle { get; set; }

        public String DescriptionArticle { get; set; }

        public SousFamille SousFamille { get; set; }

        public Marque Marque { get; set; }

        public float PrixHT { get; set; }
            
        public int Quantite { get; set; }


        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Article() { }

        /// <summary>
        /// Constructeurs avec tout les paramètres de l'article
        /// </summary>
        public Article(
            string pRefArticle,
            String PDescriptionArticle,
            SousFamille PSousFamille,
            Marque PMarque,
            float PPrixHT,
            int PQuantite)
        {
            this.RefArticle = pRefArticle;
            this.DescriptionArticle = PDescriptionArticle;
            this.SousFamille = PSousFamille;
            this.Marque = PMarque;
            this.PrixHT = PPrixHT;
            this.Quantite = PQuantite;
        }

        /// <summary>
        /// Fonction retournant la chaîne de caractère correspondant à la ligne CSV de l'article
        /// </summary>
        /// <returns>La chaîne de caractère</returns>
        public String ToCSV()
        {
            return DescriptionArticle + ";" + RefArticle + ";" + Marque.Nom + ";" + SousFamille.Famille.Nom + ";" + SousFamille.Nom + ";" + PrixHT + "\n";
        }

        /// <summary>
        /// Fonction retournant la chaîne de caractère de l'article
        /// </summary>
        /// <returns>La chaîne de caractère</returns>
        public override string ToString()
        {
            return "Données article :" + DescriptionArticle + " - " + PrixHT + " - " + Quantite;
        }

        /// <summary>
        /// Fonction retournant un tableau de string de l'article
        /// </summary>
        /// <returns>Le tableau de caractère</returns>
        public string[] ToRow()
        {
            string[] Row =
            {
                RefArticle, DescriptionArticle, SousFamille.Famille.Nom, Marque.Nom, SousFamille.Nom, PrixHT.ToString(CultureInfo.InvariantCulture),
                Quantite.ToString()
            };
            return Row;
        }
    }
}
