﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class BacchusData
    {
    
        public List<Famille> FamilleList { get; set; } = new List<Famille>();

        public List<Marque> MarqueList { get; set; } = new List<Marque>();

        public List<SousFamille> SousFamilleList { get; set; } = new List<SousFamille>();

        public List<Article> ArticleList { get; set; } = new List<Article>();


        /// <summary>
        /// Fonction ajoutant une marque
        /// </summary>
        /// <param name="Marque">La marque</param>
        public void AddMarque(Marque Marque)
        {
            MarqueList.Add(Marque);
        }

        /// <summary>
        /// Fonction retournant une marque selon sa référence
        /// </summary>
        /// <param name="RefMarque">La référence de la marque voulue</param>
        /// <returns>La marque</returns>
        public Marque GetMarque(int RefMarque)
        {
            foreach (var Marque in MarqueList)
            {
                if (Marque.RefMarque == RefMarque)
                    return Marque;
            }
            return null;
        }

        /// <summary>
        /// Fonction retournant une marque selon son nom
        /// </summary>
        /// <param name="Nom">Le nom de la marque voulue</param>
        /// <returns>La marque</returns>
        public Marque GetMarque(string Nom)
        {
            foreach (var Marque in MarqueList)
            {
                if (Marque.Nom == Nom)
                    return Marque;
            }
            return null;
        }


        /// <summary>
        /// Fonction supprimant une marque
        /// </summary>
        /// <param name="Marque">La marque</param>
        public void Remove(Marque Marque)
        {
            foreach (var Article in ArticleList)
            {
                if (Marque.Equals(Article.Marque))
                    throw new ConstraintException("La marque est encore référencée parmi des articles");
            }

            MarqueList.Remove(Marque);
        }



        /// <summary>
        /// Fonction ajoutant une famille
        /// </summary>
        /// <param name="Famille">La famille</param>
        public void AddFamille(Famille Famille)
        {
            FamilleList.Add(Famille);
        }



        /// <summary>
        /// Fonction retournant une famille selon sa référence
        /// </summary>
        /// <param name="RefFamille">La référence de la famille voulue</param>
        /// <returns>La famille</returns>
        public Famille GetFamille(int RefFamille)
        {
            foreach (var Famille in FamilleList)
            {
                if (Famille.RefFamille == RefFamille)
                    return Famille;
            }
            return null;
        }

        /// <summary>
        /// Fonction retournant une famille selon son nom
        /// </summary>
        /// <param name="Nom">Le nom de la famille voulue</param>
        /// <returns>La famille</returns>
        public Famille GetFamille(string Nom)
        {
            foreach (var Famille in FamilleList)
            {
                if (Famille.Nom == Nom)
                    return Famille;
            }
            return null;
        }


        /// <summary>
        /// Fonction supprimant une famille
        /// </summary>
        /// <param name="Famille">La famille</param>
        public void Remove(Famille Famille)
        {
            foreach (var SousFamille in SousFamilleList)
            {
                if (Famille.Equals(SousFamille.Famille))
                    throw new ConstraintException("La famille est encore référencée parmi des sous familles");
            }

            FamilleList.Remove(Famille);
        }



        /// <summary>
        /// Fonction ajoutant une sous famille
        /// </summary>
        /// <param name="SousFamille">La sous famille</param>
        public void AddSousFamille(SousFamille SousFamille)
        {
            SousFamilleList.Add(SousFamille);
        }

        /// <summary>
        /// Fonction retournant une sous famille selon sa référence
        /// </summary>
        /// <param name="RefSousFamille">La référence de la sous famille voulue</param>
        /// <returns>La sous famille</returns>
        public SousFamille GetSousFamille(int RefSousFamille)
        {
            foreach (var SousFamille in SousFamilleList)
            {
                if (SousFamille.RefSousFamille == RefSousFamille)
                    return SousFamille;
            }
            return null;
        }

        /// <summary>
        /// Fonction retournant une sous famille selon son nom
        /// </summary>
        /// <param name="Nom">Le nom de la sous famille voulue</param>
        /// <returns>La sous famille</returns>
        public SousFamille GetSousFamille(string Nom)
        {
            foreach (var SousFamille in SousFamilleList)
            {
                if (SousFamille.Nom == Nom)
                    return SousFamille;
            }
            return null;
        }


        /// <summary>
        /// Fonction supprimant une sous famille
        /// </summary>
        /// <param name="SousFamille">La sous famille</param>
        public void Remove(SousFamille SousFamille)
        {
            foreach (var Article in ArticleList)
            {
                if (SousFamille.Equals(Article.SousFamille))
                    throw new ConstraintException("La sous famille est encore référencée parmi des articles");
            }

            SousFamilleList.Remove(SousFamille);
        }



        /// <summary>
        /// Fonction ajoutant un article
        /// </summary>
        /// <param name="Article">L'article</param>
        public void AddArticle(Article Article)
        {
            ArticleList.Add(Article);
        }

        /// <summary>
        /// Fonction retournant un article selon sa référence
        /// </summary>
        /// <param name="RefArticle">La référence de la article voulue</param>
        /// <returns>L'article</returns>
        public Article GetArticle(string RefArticle)
        {
            foreach (var Article in ArticleList)
            {
                if (Article.RefArticle.Equals(RefArticle))
                    return Article;
            }

            return null;
        }


        /// <summary>
        /// Fonction supprimant un article
        /// </summary>
        /// <param name="Article">L'article</param>
        public void Remove(Article Article)
        {
            ArticleList.Remove(Article);
        }
    }

}
