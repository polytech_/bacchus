﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Model;
using DAOService;
using System.Drawing.Configuration;

namespace Bacchus
{
    public partial class FormMain : Form
    {
        //private ListViewColumnSorter ColumnSorter;

        public BacchusData BacchusData { get; private set; }

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public FormMain()
        {
            InitializeComponent();

            object SettingPos = Properties.Settings.Default;

            if(SettingPos != null && SettingPos is Point)
            {
                Location = (Point) SettingPos;
            }



            CenterToScreen();
        }

        /// <summary>
        /// Event du click sur le sous menu Importer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void importerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormImport formImport = new FormImport();
            //Center of the parent window
            formImport.StartPosition = FormStartPosition.CenterParent;
            formImport.ShowDialog(this);
        }

        /// <summary>
        /// Méthode de chargement de la fenêtre principale
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {

            //On sauvegarde la position et l'état de la fenêtre quand elle se charge 
            //(prérequis : ajout du paramètre de position et d'état dans le fichier Settings.settings sous Propriétés)
            Location = Properties.Settings.Default.F1Location;
            WindowState = Properties.Settings.Default.F1State;

            //On initialise les élements de la fenêtre et notre classe d'historique
            BacchusData = DAO.InitialiseAll();
            UpdateTreeView();
            UpdateListView();
            UpdateNbArticles(BacchusData.ArticleList.Count);

        }


        /// <summary>
        /// Ajoute un noeud avec le nom de la famille passée en paramètre
        /// </summary>
        /// <param name="Famille">la famille à ajouter</param>
        public void AddNodeFamille(Famille Famille)
        {
            TreeView.Nodes[1].Nodes.Add("Famille", Famille.Nom);
        }

        /// <summary>
        /// Ajoute un noeud avec le nom de la sous-famille passée en paramètre
        /// </summary>
        /// <param name="SousFamille">la sous-famille à ajouter</param>
        public void AddNodeSousFamille(SousFamille SousFamille) {

            var incrementNode = 0;

            foreach (var famille in BacchusData.FamilleList)
            {
                foreach (var sousfamille in BacchusData.SousFamilleList)
                {
                    if (sousfamille.Famille.Nom.Equals(SousFamille.Famille.Nom))
                    {
                        TreeView.Nodes[1].Nodes[incrementNode].Nodes.Add("SousFamille", SousFamille.Nom);
                    }
                }

                incrementNode++;
            }

        }

        /// <summary>
        /// Met à jour la treeView en fonction des changements apportés aux données (import/suppression etc)
        /// </summary>
        public void UpdateTreeView()
        {
            //On vérifie si les noeuds on déjà été créés ensuite on les crée
            if (!TreeView.Nodes.ContainsKey("Articles") &&
               !TreeView.Nodes.ContainsKey("Familles") &&
               !TreeView.Nodes.ContainsKey("Marques"))
            {
                TreeView.Nodes.Add("Articles", "Tout les articles");
                TreeView.Nodes.Add("Familles", "Familles");
                TreeView.Nodes.Add("Marques", "Marques");
            }
        
            var incrementNode = 0;

            foreach(var famille in BacchusData.FamilleList)
            {
                

                TreeView.Nodes[1].Nodes.Add("Famille", famille.Nom);

                foreach(var sousfamille in BacchusData.SousFamilleList)
                {
                    if (sousfamille.Famille.Nom.Equals(famille.Nom))
                    {
                        TreeView.Nodes[1].Nodes[incrementNode].Nodes.Add("SousFamille", sousfamille.Nom);
                    }
                }

                incrementNode++;
            }

            foreach (var marque in BacchusData.MarqueList)
            {
                TreeView.Nodes[2].Nodes.Add("Marque", marque.Nom);
            }
        }

        /// <summary>
        /// Supprime toutes les données dans le cas d'un import en mode écrasement par exemple
        /// </summary>
        public void ClearAll()
        {
            //Clear de la treeview
            TreeView.Nodes.Clear();

            //Clear de la listview
            ListView.Clear();

            //Remise à 0 des données
            BacchusData.MarqueList = new List<Marque>();
            BacchusData.SousFamilleList = new List<SousFamille>();
            BacchusData.FamilleList = new List<Famille>();
            BacchusData.ArticleList = new List<Article>();
        }


        /// <summary>
        /// Met à jour la listView en fonction des changements apportés aux données (import/suppression etc)
        /// </summary>
        public void UpdateListView()
        {
            ListView.Clear();
            var SelectedNode = TreeView.SelectedNode;

            ListView.Groups.Clear();
            ListView.View = View.Details;
            /*if (ListView.View.Equals(View.Tile))
            {
                ListView.Groups.Clear();
                ListView.View = View.Details;
            }*/

            ListView.Hide();
            if(SelectedNode != null)
            {
                if (SelectedNode.Name.Equals("Marque"))
                {
                    ListView.Columns.Add("Référence", "Référence");
                    ListView.Columns.Add("Description", "Description");
                    ListView.Columns.Add("Famille", "Famille");
                    ListView.Columns.Add("Marque", "Marque");
                    ListView.Columns.Add("Sous-Famille", "Sous-Famille");
                    ListView.Columns.Add("Prix", "Prix H.T.");
                    ListView.Columns.Add("Quantité", "Quantité");
                    foreach (var Article in BacchusData.ArticleList)
                    {
                        if(Article.Marque.Nom.Equals(SelectedNode.Text))
                        {
                            ListView.Items.Add(new ListViewItem(Article.ToRow()));
                        }
                    }
                    //On fait en sorte que chaque colonne de la listView ait la taille de la plus longue cellule
                    //(même chose pour les autres types de données (article, famille, sous-famille)
                    ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    ListView.Text = @"ArticlesMarque";

                }
                else if (SelectedNode.Name.Equals("Famille"))
                {
                    ListView.Columns.Add("Description", "Description");
                    foreach (var SousFamille in BacchusData.SousFamilleList)
                    {
                        if (SousFamille.Famille.Nom.Equals(SelectedNode.Text))
                        {
                            ListView.Items.Add(new ListViewItem(SousFamille.ToRow()));
                        }
                    }
                    ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    ListView.Text = @"Sous-Famille";
                }
                else if (SelectedNode.Name.Equals("SousFamille"))
                {
                    ListView.Columns.Add("Référence", "Référence");
                    ListView.Columns.Add("Description", "Description");
                    ListView.Columns.Add("Famille", "Famille");
                    ListView.Columns.Add("Marque", "Marque");
                    ListView.Columns.Add("Sous-Famille", "Sous-Famille");
                    ListView.Columns.Add("Prix", "Prix H.T.");
                    ListView.Columns.Add("Quantité", "Quantité");
                    foreach (var Article in BacchusData.ArticleList)
                    {
                        if (Article.SousFamille.Nom.Equals(SelectedNode.Text))
                        {
                            ListView.Items.Add(new ListViewItem(Article.ToRow()));
                        }
                    }
                    ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    ListView.Text = @"ArticlesSF";

                }
                else if (SelectedNode.Name.Equals("Familles"))
                {                    
                    ListView.Columns.Add("Description", "Description");
                    foreach (var Famille in BacchusData.FamilleList)
                    {
                        ListView.Items.Add(new ListViewItem(Famille.ToRow()));
                    }
                    ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    ListView.Text = @"Familles";

                }
                else if (SelectedNode.Name.Equals("Marques"))
                {
                    ListView.Columns.Add("Description", "Description");
                    foreach (var Marque in BacchusData.MarqueList)
                    {
                        ListView.Items.Add(new ListViewItem(Marque.ToRow()));
                    }
                    ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    ListView.Text = @"Marques";
                }
                else if (SelectedNode.Name.Equals("Articles"))
                {
                    ListView.Columns.Add("Référence", "Référence");
                    ListView.Columns.Add("Description", "Description");
                    ListView.Columns.Add("Famille", "Famille");
                    ListView.Columns.Add("Marque", "Marque");
                    ListView.Columns.Add("Sous-Famille", "Sous-Famille");
                    ListView.Columns.Add("Prix", "Prix H.T.");
                    ListView.Columns.Add("Quantité", "Quantité");

                    foreach (var Article in BacchusData.ArticleList)
                    {
                        ListView.Items.Add(new ListViewItem(Article.ToRow()));
                    }

                    ListView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    ListView.Text = @"Articles";
                }
            }


            ListView.Show();

        }

        /// <summary>
        /// Event du click sur le sous menu Exporter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exporterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormExport formExport = new FormExport();
            //Center of the parent window
            formExport.StartPosition = FormStartPosition.CenterParent;
            formExport.ShowDialog(this);
        }

        /// <summary>
        /// Event du click sur un élement de la treeview => Met à jour les données en fonction de l'élément sélectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            UpdateListView();
        }

        /// <summary>
        /// Event du click sur le sous menu Actualiser
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void actualiserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Refresh();
        }

        /// <summary>
        /// Met à jour les données après un changement ou autre
        /// </summary>
        public void Refresh()
        {
            ClearAll();
            BacchusData = DAO.InitialiseAll();
            UpdateTreeView();
            UpdateListView();
            UpdateNbArticles(BacchusData.ArticleList.Count);
        }

        /// <summary>
        /// Met à jour le nbre d'articles dans le statusStrip
        /// </summary>
        /// <param name="NbArticles"> Le nombre d'articles</param>
        public void UpdateNbArticles(int NbArticles)
        {
            ToolStripStatusLabel1.Text = @"Il y a " + NbArticles + @" articles dans la base de données";
        }

        /// <summary>
        /// Update la listVieW quand on presse la touche F5, Une pop up s'ouvre si on appuye 
        /// sur Enter ou Suppr et propose soit de supprimer ou modifier le premier élement
        /// de la listView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F5:
                    UpdateListView();
                    break;
                case Keys.Delete:
                    DeleteElement(ListView.SelectedItems[0]);
                    break;
                case Keys.Enter:
                    ModifyElement(ListView.SelectedItems[0]);
                    break;
            }
        }


        /// <summary>
        /// Modifie un élement de la listView 
        /// </summary>
        /// <param name="Item"> L'item à modifier</param>
        private void ModifyElement(ListViewItem Item)
        {
            //Si on affiche le menu contextuel à partir des articles
            if (ListView.Text.Equals("Articles"))
            {
                var Article = BacchusData.GetArticle(Item.Text);

                var ModifyArticleForm = new ModifyArticleForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this
                };

                ModifyArticleForm.InitData(Article);
                ModifyArticleForm.ShowDialog(this);
            }
            //Si on affiche le menu contextuel à partir des marques
            else if (ListView.Text.Equals("Marques"))
            {
                var Marque = BacchusData.GetMarque(Item.Text);

                var ModifyMarqueForm = new ModifyMarqueForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this,
                };
                ModifyMarqueForm.InitData(Marque);
                ModifyMarqueForm.ShowDialog(this);
            }
            //Si on affiche le menu contextuel à partir des familles
            else if (ListView.Text.Equals("Familles"))
            {
                var Famille = BacchusData.GetFamille(Item.Text);
                var ModifyFamilleForm = new ModifyFamilleForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this
                };

                ModifyFamilleForm.InitData(Famille);
                ModifyFamilleForm.ShowDialog(this);
            }
            //Si on affiche le menu contextuel à partir des sous-familles
            else if (ListView.Text.Equals("Sous-Famille"))
            {
                var SousFamille = BacchusData.GetSousFamille(Item.Text);
                var ModifySousFamilleForm = new ModifySousFamilleForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this
                };

                ModifySousFamilleForm.InitData(SousFamille);
                ModifySousFamilleForm.ShowDialog(this);
            }
        }

        /// <summary>
        /// Supprime un élement de la listView 
        /// </summary>
        /// <param name="Item"> L'item à supprimer</param>
        private void DeleteElement(ListViewItem Item)
        {

            if (ListView.Text.Equals("Articles"))
            {
                /*
                 * On est entrain de supprimer un article
                 */
                var Result = MessageBox.Show(@"Êtes-vous sûr de vouloir supprimer l'article " + Item.Text + @" ?",
                    @"Suppression d'un article", MessageBoxButtons.YesNo);

                if (Result == DialogResult.No) return;

                // Si l'utilisateur clique sur Yes alors on supprime
                var Article = BacchusData.GetArticle(Item.Text);
                if (Article == null)
                    throw new NullReferenceException("L'article à supprimer n'existe pas.");

                BacchusData.Remove(Article);
                DAO.Delete(Article);

                UpdateNbArticles(BacchusData.ArticleList.Count);
                UpdateListView();
            }

            else if (ListView.Text.Equals("Marques"))
            {
         
                var Marque = BacchusData.GetMarque(Item.Text);
                if (Marque == null)
                    throw new NullReferenceException("La marque à supprimer n'existe pas.");

                //Booléen nous permettant de vérifier si il y a des articles liés à la marque que l'utilisateur veut supprimer
                var ExistsMarques = false;

                foreach (Article Article in BacchusData.ArticleList)
                {
                    if (Article.Marque.Nom.Equals(Marque.Nom))
                    {
                        ExistsMarques = true;
                    }
                }

                if(!ExistsMarques) {

                    var DecisionDeleteMarque = MessageBox.Show("Êtes vous sûr(e) de vouloir supprimer la marque" + Item.Text + @" ?",
                        @"Suppression d'une marque", MessageBoxButtons.YesNo);

                    if (DecisionDeleteMarque == DialogResult.No) return;

                    // Si l'utilisateur clique sur Yes alors on supprime
                    try
                    { 
                        //On supprime ensuite la marque
                        BacchusData.Remove(Marque);
                        DAO.Delete(Marque);
                        Refresh();

                    }

                    catch (Exception Exception)
                    {
                        MessageBox.Show(Exception.Message, @"Suppression d'un article (Erreur)", MessageBoxButtons.OK);
                    }

                }

                else
                {
                    MessageBox.Show(@"La marque contient un ou plusieurs article(s), suppression impossible.",
                    @"Suppression d'une marque", MessageBoxButtons.OK);
                }


            }

            else if (ListView.Text.Equals("Familles"))
            {
                var Famille = BacchusData.GetFamille(Item.Text);
                if (Famille == null)
                    throw new NullReferenceException("La famille à supprimer n'existe pas.");

                var ExistsFamilles = false;

                foreach (SousFamille SousFamille in BacchusData.SousFamilleList)
                {
                    if (SousFamille.Famille.Nom.Equals(Famille.Nom))
                    {
                        ExistsFamilles = true;
                    }
                }
                
                if(!ExistsFamilles)
                {
                    var DecisionDeleteFamille = MessageBox.Show(@"Êtes-vous sûr(e) de vouloir supprimer la famille " + Item.Text + @" ?",
                    @"Suppression d'une famille", MessageBoxButtons.YesNo);

                    if (DecisionDeleteFamille == DialogResult.No) return;

                    // Si l'utilisateur clique sur Yes alors on supprime
                    try
                    {
                        BacchusData.Remove(Famille);
                        DAO.Delete(Famille);
                        Refresh();
                    }
                    catch (Exception Exception)
                    {
                        MessageBox.Show(Exception.Message, @"Suppression d'une famille", MessageBoxButtons.OK);
                    }
                }

                else
                {
                    MessageBox.Show(@"La famille contient un ou des sous-familles, suppression impossible.",
                    @"Suppression d'une famille", MessageBoxButtons.OK);
                }

            }

              
            else if (ListView.Text.Equals("Sous-Famille"))
            {
                // Si l'utilisateur clique sur Yes alors on supprime
                var SousFamille = BacchusData.GetSousFamille(Item.Text);

                if (SousFamille == null)
                    throw new NullReferenceException("La sous-famille à supprimer n'existe pas.");

                var ExistsSF = false;

                foreach (Article Article in BacchusData.ArticleList)
                {
                    if (Article.SousFamille.Nom.Equals(SousFamille.Nom))
                    {
                        ExistsSF = true;
                    }
                }


                if (!ExistsSF)
                {

                    var DecisionDeleteSF = MessageBox.Show(@"Êtes-vous sûr de vouloir supprimer la sous-famille " + Item.Text + @" ?",
                    @"Suppression d'une sous-famille", MessageBoxButtons.YesNo);

                    if (DecisionDeleteSF == DialogResult.No) return;

                    // Si l'utilisateur clique sur Yes alors on supprime
                    try
                    {
                        BacchusData.Remove(SousFamille);
                        DAO.Delete(SousFamille);
                        Refresh();
                    }
                    catch (Exception Exception)
                    {
                        MessageBox.Show(Exception.Message, @"Suppression d'une sous-famille", MessageBoxButtons.OK);
                    }

                }

                else
                {
                    MessageBox.Show(@"La sous-famille contient un ou plusieurs articles, suppression impossible.",
                    @"Suppression d'une sous-famille", MessageBoxButtons.OK);
                }            
                
            }
        }

        /// <summary>
        /// Event du click droit sur un élement de la listView
        /// ouvre un menu permettant de ajouter/modifier/supprimer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_MouseClick(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Right)
            {
                if (ListView.FocusedItem.Bounds.Contains(e.Location))
                {
                    ContextMenuStripListView.Show(Cursor.Position);
                }
            }

        }

        /// <summary>
        /// Event du click sur une colonne de la listView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ListView.Items.Clear();

            //Tile view (on affiche que les description des articles)
            ListView.View = View.Details;

            //Si On clique sur la colonne Description, on trie en fonction de la première lettre
            if (e.Column == 1)
            {

                //list de groupes (alphabet)
                List<ListViewGroup> AlphabetGroups = new List<ListViewGroup>();

                //Char représentant la lettre de l'alphabet
                char Letter = 'A';

                for (int Index = 0; Index < 26; Index++)
                {
                    //String pour incrémenter les lettre de l'alphabet
                    string StringLetter = Letter + "";
                    AlphabetGroups.Add(new ListViewGroup(StringLetter, HorizontalAlignment.Left));
                    Letter++;
                }

                //Ajout des données d'articles
                foreach (Article Article in BacchusData.ArticleList)
                {
                    foreach (ListViewGroup group in AlphabetGroups)
                    {
                        if (group.Header[0] == Article.DescriptionArticle[0])
                        {
                            ListView.Items.Add(new ListViewItem(Article.ToRow(), group));
                        }
                    }
                }

                //Ajout des groupes
                foreach (ListViewGroup Group in AlphabetGroups)
                {
                    ListView.Groups.Add(Group);
                }
            }
            //Colonne Famille
            else if (e.Column == 2)
            {
                List<ListViewGroup> FamilyGroups = new List<ListViewGroup>();

                //groups
                foreach (Famille sf in BacchusData.FamilleList)
                {
                    FamilyGroups.Add(new ListViewGroup(sf.Nom, HorizontalAlignment.Left));

                }

                //Ajout des données d'articles
                foreach (Article Article in BacchusData.ArticleList)
                {
                    foreach (Famille Famille in BacchusData.FamilleList)
                    {
                        if (Famille.Nom.Equals(Article.SousFamille.Famille.Nom))
                        {
                            foreach (ListViewGroup Group in FamilyGroups)
                            {
                                if (Group.Header == Famille.Nom)
                                {
                                    ListView.Items.Add(new ListViewItem(Article.ToRow(), Group));
                                }
                            }
                        }

                    }
                }

                //Ajout des groupes
                foreach (ListViewGroup Group in FamilyGroups)
                {
                    ListView.Groups.Add(Group);
                }

            }

            //Colonne Marque
            else if (e.Column == 3)
            {

                List<ListViewGroup> MarkGroups = new List<ListViewGroup>();

                //On génère la liste des groupes (comme avant)
                foreach (Marque Marque in BacchusData.MarqueList)
                {
                    MarkGroups.Add(new ListViewGroup(Marque.Nom, HorizontalAlignment.Left));
                }


                //On ajoute les données des articles
                foreach (Article Article in BacchusData.ArticleList)
                {
                    foreach (Marque Marque in BacchusData.MarqueList)
                    {
                        if (Marque.Nom.Equals(Article.Marque.Nom))
                        {
                            foreach (ListViewGroup Group in MarkGroups)
                            {
                                if (Group.Header == Marque.Nom)
                                {
                                    ListView.Items.Add(new ListViewItem(Article.ToRow(), Group));
                                }
                            }
                        }
                    }
                }

                //Ajout des groupes
                foreach (ListViewGroup Group in MarkGroups)
                {
                    ListView.Groups.Add(Group);
                }
            }
            else if (e.Column == 4)
            {

                List<ListViewGroup> Groups = new List<ListViewGroup>();

                //On ajoute les groupes dans la liste des groupes
                foreach (SousFamille SousFamille in BacchusData.SousFamilleList)
                {
                    Groups.Add(new ListViewGroup(SousFamille.Nom, HorizontalAlignment.Left));
                }

                //Ajout des articles
                foreach (Article Article in BacchusData.ArticleList)
                {
                    foreach (SousFamille SousFamille in BacchusData.SousFamilleList)
                    {
                        if (SousFamille.Nom.Equals(Article.SousFamille.Nom))
                        {
                            foreach (ListViewGroup Group in Groups)
                            {
                                if (Group.Header == SousFamille.Nom)
                                {
                                    ListView.Items.Add(new ListViewItem(Article.ToRow(), Group));
                                }
                            }
                        }

                    }
                }

                //Ajout des groupes
                foreach (ListViewGroup Group in Groups)
                {
                    ListView.Groups.Add(Group);
                }
            }

            else
            {
                MessageBox.Show(@"Regroupement impossible, merci de regrouper par Description, Marque, Famille ou Sous-Famille.",
                    @"Erreur", MessageBoxButtons.OK);

            }

            ListView.Sort();
        }

        /// <summary>
        /// Event du click sur le menu Supprimer du menu qui s'affiche lorsque
        /// l'on clique sur un élement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void supprimerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteElement(ListView.SelectedItems[0]);
        }

        /// <summary>
        /// Event du click sur le menu Modifier du menu qui s'affiche lorsque
        /// l'on clique sur un élement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void modifierToolStripMenuItem_Click(object sender, EventArgs e)
        {

            ModifyElement(ListView.SelectedItems[0]);
        }

        /// <summary>
        /// Event du double click sur un élement de la listView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ListView_DoubleClick(object sender, EventArgs e)
        {
            ModifyElement(ListView.SelectedItems[0]);
        }

        /// <summary>
        /// Event du click sur le menu Ajouter du menu qui s'affiche lorsque
        /// l'on clique sur un élement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ajouterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Si on affiche le menu contextuel à partir des articles
            if (ListView.Text.Equals("Articles"))
            {
                var AjoutArticleForm = new AjoutArticleForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this
                };
                AjoutArticleForm.Init();
                AjoutArticleForm.ShowDialog(this);
            }
            //Si on affiche le menu contextuel à partir des marques
            else if (ListView.Text.Equals("Marques"))
            {
                var AjoutMarqueForm = new AjoutMarqueForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this,
                };
                AjoutMarqueForm.ShowDialog(this);
            }
            //Si on affiche le menu contextuel à partir des familles
            else if (ListView.Text.Equals("Familles"))
            {
                var AjoutFamilleForm = new AjoutFamilleForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this
                };
                AjoutFamilleForm.ShowDialog(this);
            }
            //Si on affiche le menu contextuel à partir des sous-famille
            else if (ListView.Text.Equals("Sous-Famille"))
            {
                var AjoutSousFamilleForm = new AjoutSousFamilleForm
                {
                    StartPosition = FormStartPosition.CenterParent,
                    Owner = this
                };
                AjoutSousFamilleForm.Init();
                AjoutSousFamilleForm.ShowDialog(this);
            }
        }

        /// <summary>
        /// Méthode exécutée lors de la fermeture de la FormMain et servant à enregistrer la position et l'état de la fenêtre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            //On sauvegarde la position et l'état de la fenêtre sous Default avant de fermer 
            Properties.Settings.Default.F1State = WindowState;
            Properties.Settings.Default.F1Location = Location;

            //On enregistre
            Properties.Settings.Default.Save();
        }
    }
}
