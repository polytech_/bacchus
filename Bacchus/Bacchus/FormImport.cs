﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using IOCSV;
using Model;

namespace Bacchus
{
   
    public partial class FormImport : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public FormImport()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Importe les données en mode écrasement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EcrModeBtn_Click(object sender, EventArgs e)
        {
            //Si l'utilisateur ne fait rentrer aucun fichier on affiche un messagebox d'attention lui disant d'insérer un fichier
            if(FilePathInput.Text.Equals(string.Empty))
            {
                MessageBox.Show(@"Vous n'avez pas sélectionné de fichier, veuillez en insérer un", @"Attention", MessageBoxButtons.OK);
                return;
            }

            //On vide la listview, la treeview et notre conteneur de données BacchusData
            ((FormMain)Owner).ClearAll();

            //On réalise l'importation en mode écrasement (booléen à true)
            int NumberOfLines = FileOperations.ImportCSV(FilePathInput.Text, true, ((FormMain)Owner).BacchusData, ProgressBar);

            //On avertit l'utilisateur que l'importation est réussie et du nombre d'articles ajoutés
            MessageBox.Show(@"Fichier importé dans la base de données, "+ NumberOfLines + " articles ajoutés !"
                , @"Succès", MessageBoxButtons.OK);

            //On initialise la treeview et la listview pour l'affichage dans la fenêtre principale
            ((FormMain)Owner).UpdateTreeView();
            ((FormMain)Owner).UpdateListView();
            ((FormMain)Owner).UpdateNbArticles(NumberOfLines);

            Close();

        }

        /// <summary>
        /// Ouvre une fenetre pour sélectionner un fichier .csv à importer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseFileBtn_Click(object sender, EventArgs e)
        {
            var OpenFileDialog = new OpenFileDialog { Filter = @"CSV files (*.csv)|*.csv" };

            if (OpenFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            var FilePath = OpenFileDialog.FileName;
            FilePathInput.Text = FilePath;

        }

        /// <summary>
        /// met à jour la valeur de la barre de progression
        /// </summary>
        private void UpdateProgress(int Progress)
        {
            ProgressBar.Value = Progress;
        }

        /// <summary>
        /// modifie la valeur maximale de la barre de progression
        /// </summary>
        private void SetMaxValue(int MaxValue)
        {
            ProgressBar.Maximum = MaxValue;
        }

        /// <summary>
        /// Importe les données en mode ajout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AjModeBtn_Click(object sender, EventArgs e)
        {
            if (FilePathInput.Text.Equals(string.Empty))
            {
                MessageBox.Show(@"Vous n'avez pas sélectionné de fichier, veuillez en insérer un", @"Attention", MessageBoxButtons.OK);
                return;
            }

            //On récupère l'ancien nombre d'articles importés avant de l'écraser
            int DataHistory = ((FormMain)Owner).BacchusData.ArticleList.Count();

            //On récupère le nombre d'articles importés
            int NumberOfLines = FileOperations.ImportCSV(FilePathInput.Text, false, ((FormMain)Owner).BacchusData, ProgressBar);

            //On soustrait le nombre d'articles qu'il y avait du nombre d'article qu'il y a dans le nouveau fichier
            int NbArticlesAdded = NumberOfLines - DataHistory + 1;

            MessageBox.Show(@"Fichier importé dans la base de données, " + NbArticlesAdded + " articles ajoutés !"
                , @"Succès", MessageBoxButtons.OK);

            //On initialise la treeview et la listview
            ((FormMain)Owner).UpdateTreeView();
            ((FormMain)Owner).UpdateListView();
            ((FormMain)Owner).UpdateNbArticles(NumberOfLines);

            Close();
        }
    }
}
