﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DAOService;
using Model;
using Bacchus;
using System.Windows.Forms;

namespace IOCSV
{
    class FileOperations
    {

        /// <summary>
        /// Fonction exportant la base de données dans un fichier CSV
        /// </summary>
        /// <param name="FilePath">Le nom du fichier CSV</param>
        /// <param name="BacchusData">Le modèle à exporter</param>
        /// <param name="ProgressBar">La barre de progression</param>
        public static void ExportCsv(string FilePath, BacchusData BacchusData, ProgressBar ProgressBar)
        {
            ProgressBar.Maximum = BacchusData.ArticleList.Count();

            using (StreamWriter sr = new StreamWriter(new FileStream(FilePath, FileMode.OpenOrCreate, FileAccess.Write), Encoding.UTF8))
            {
                sr.Write("Description;Ref;Marque;Famille;Sous-Famille;Prix H.T.\n");
                var CurrentArticle = 0;
                foreach (var Article in BacchusData.ArticleList)
                {
                    sr.Write(Article.ToCSV());

                    CurrentArticle++;
                    ProgressBar.PerformStep();
                    
                }

                sr.Close();

            }
        }


        /// <summary>
        /// Fonction chargeant un fichier CSV dans la base de données
        /// </summary>
        /// <param name="filePath">Le nom du fichier CSV à charger</param>
        /// <param name="mode">Si vrai alors on supprime les données existantes de la base de données</param>
        /// <param name="BacchusData">Le modèle existant</param>
        /// <param name="ProgressBar">La barre de progression</param>
        /// <returns>Le nombre de lignes dans le fichier</returns>
        public static int ImportCSV(string filePath, bool mode, BacchusData BacchusData, ProgressBar ProgressBar)
        {
            if(mode)
            {
                DAO.ClearAllTables();
            }

            int lineCount = File.ReadLines(filePath).Count() - 1;

            ProgressBar.Maximum = lineCount;

            ProgressBar.Step = lineCount / 20;


            using (StreamReader sr = new StreamReader(filePath, Encoding.Default))
            {

                //On saute la ligne des entêtes
                string headerLine = sr.ReadLine();

                //Variable d'itération des lignes
                string line;

                while ((line = sr.ReadLine()) != null)
                {

                    List<string> lineValues = line.Split(';').ToList();

                    // On récupère chaque colonne en retirant les espaces
                    string Description = lineValues[0].Trim();
                    string RefArticle = lineValues[1].Trim();
                    string NomMarque = lineValues[2].Trim();
                    string NomFamille = lineValues[3].Trim();
                    string NomSousFamille = lineValues[4].Trim();
                    float PrixHt = float.Parse(lineValues[5].Trim());

                    //Vérification si la marque existe
                    var Marque = BacchusData.GetMarque(NomMarque);
                    if(Marque == null)
                    {
                        Marque = DAO.AddMarque(new Marque(0, NomMarque));
                        BacchusData.AddMarque(Marque);
                    }

                    //Vérification si la famille existe
                    var Famille = BacchusData.GetFamille(NomFamille);
                    if (Famille == null)
                    {
                        Famille = DAO.AddFamille(new Famille(0, NomFamille));
                        BacchusData.AddFamille(Famille);
                    }

                    //Vérification si la sous-famille existe
                    var SousFamille = BacchusData.GetSousFamille(NomSousFamille);
                    if (SousFamille == null)
                    {
                        SousFamille = DAO.AddSousFamille(new SousFamille(0, Famille, NomSousFamille));
                        BacchusData.AddSousFamille(SousFamille);
                    }

                    //On ajoute l'article s'il n'existe pas dans la base de données
                    var Article = BacchusData.GetArticle(RefArticle);
                    if (Article == null)
                    {
                        Article = DAO.AddArticle(new Article(RefArticle, Description, SousFamille, Marque, PrixHt, 1));
                        BacchusData.AddArticle(Article);
                    }

                    ProgressBar.PerformStep();
                }
            
                return lineCount;
            }
        }
    }
}
 