﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class AjoutMarqueForm : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public AjoutMarqueForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event de l'ajout du nouvelle famille
        /// Vérifie que le champ du nom de la marque 
        /// ne soit pas vide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            // On vérifie que le nom de la marque est bien saisi
            if (NomMarqueTextBox.Text.Equals(""))
            {
                MessageBox.Show(@"Vous n'avez pas saisi de nom pour la marque", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                var TempMarque = ((FormMain)Owner).BacchusData.GetMarque(NomMarqueTextBox.Text);

                // On vérifie que le nom de la marque saisi n'existe pas déjà
                if (TempMarque != null)
                {
                    MessageBox.Show(@"Une marque avec ce nom existe déjà", @"Attention", MessageBoxButtons.OK);
                }
                else
                {
                    // On crée la marque si toutes les conditions sont validées
                    var Marque = new Marque(0, NomMarqueTextBox.Text);
                    try
                    {
                        DAO.AddMarque(Marque);
                        ((FormMain)Owner).BacchusData.AddMarque(Marque);
                        ((FormMain)Owner).Refresh();
                        Close();
                    }
                    catch (Exception Exception)
                    {
                        Console.Write(Exception.Message);
                        MessageBox.Show(@"La référence et/ou le nom de la marque est déjà utilisée", @"Attention",
                            MessageBoxButtons.OK);
                    }
                }
            }

        }

        /// <summary>
        /// Event de la fermeture de la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
