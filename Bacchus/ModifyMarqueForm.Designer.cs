﻿namespace Bacchus
{
    partial class ModifyMarqueForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.RefMarqueTextBox = new System.Windows.Forms.TextBox();
            this.NomMarqueTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.AbortButton = new System.Windows.Forms.Button();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(153, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Référence de la marque :";
            // 
            // RefMarqueTextBox
            // 
            this.RefMarqueTextBox.Enabled = false;
            this.RefMarqueTextBox.Location = new System.Drawing.Point(187, 30);
            this.RefMarqueTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RefMarqueTextBox.Name = "RefMarqueTextBox";
            this.RefMarqueTextBox.Size = new System.Drawing.Size(203, 22);
            this.RefMarqueTextBox.TabIndex = 1;
            // 
            // NomMarqueTextBox
            // 
            this.NomMarqueTextBox.Location = new System.Drawing.Point(187, 74);
            this.NomMarqueTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NomMarqueTextBox.Name = "NomMarqueTextBox";
            this.NomMarqueTextBox.Size = new System.Drawing.Size(203, 22);
            this.NomMarqueTextBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(31, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nom de la marque :";
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(217, 130);
            this.AbortButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(87, 28);
            this.AbortButton.TabIndex = 18;
            this.AbortButton.Text = "Annuler";
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.AbortButton_Click);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(122, 130);
            this.ConfirmButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(87, 28);
            this.ConfirmButton.TabIndex = 17;
            this.ConfirmButton.Text = "Confirmer";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // ModifyMarqueForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 174);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.NomMarqueTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RefMarqueTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(441, 213);
            this.MinimumSize = new System.Drawing.Size(441, 213);
            this.Name = "ModifyMarqueForm";
            this.Text = "Modifier une marque";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RefMarqueTextBox;
        private System.Windows.Forms.TextBox NomMarqueTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.Button ConfirmButton;
    }
}