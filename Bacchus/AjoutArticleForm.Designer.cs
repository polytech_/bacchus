﻿namespace Bacchus
{
    partial class AjoutArticleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AbortButton = new System.Windows.Forms.Button();
            this.AddButton = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.QteTextBox = new System.Windows.Forms.TextBox();
            this.DescriptionRTextBox = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.PrixTextBox = new System.Windows.Forms.TextBox();
            this.SousFamilleComboBox = new System.Windows.Forms.ComboBox();
            this.FamilleComboBox = new System.Windows.Forms.ComboBox();
            this.MarqueComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.RefTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(229, 506);
            this.AbortButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(87, 28);
            this.AbortButton.TabIndex = 32;
            this.AbortButton.Text = "Annuler";
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.AbortButton_Click);
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(134, 506);
            this.AddButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(87, 28);
            this.AddButton.TabIndex = 31;
            this.AddButton.Text = "Ajouter";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(75, 441);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 16);
            this.label7.TabIndex = 30;
            this.label7.Text = "Quantité :";
            // 
            // QteTextBox
            // 
            this.QteTextBox.Location = new System.Drawing.Point(175, 437);
            this.QteTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.QteTextBox.Name = "QteTextBox";
            this.QteTextBox.Size = new System.Drawing.Size(175, 22);
            this.QteTextBox.TabIndex = 29;
            this.QteTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.QteTextBox_KeyPress);
            // 
            // DescriptionRTextBox
            // 
            this.DescriptionRTextBox.Location = new System.Drawing.Point(175, 318);
            this.DescriptionRTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DescriptionRTextBox.Name = "DescriptionRTextBox";
            this.DescriptionRTextBox.Size = new System.Drawing.Size(175, 85);
            this.DescriptionRTextBox.TabIndex = 28;
            this.DescriptionRTextBox.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(75, 354);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 16);
            this.label6.TabIndex = 27;
            this.label6.Text = "Description :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 26;
            this.label5.Text = "Prix H.T :";
            // 
            // PrixTextBox
            // 
            this.PrixTextBox.Location = new System.Drawing.Point(175, 256);
            this.PrixTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PrixTextBox.Name = "PrixTextBox";
            this.PrixTextBox.Size = new System.Drawing.Size(175, 22);
            this.PrixTextBox.TabIndex = 25;
            this.PrixTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.PrixTextBox_KeyPress);
            // 
            // SousFamilleComboBox
            // 
            this.SousFamilleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SousFamilleComboBox.FormattingEnabled = true;
            this.SousFamilleComboBox.Location = new System.Drawing.Point(175, 197);
            this.SousFamilleComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.SousFamilleComboBox.Name = "SousFamilleComboBox";
            this.SousFamilleComboBox.Size = new System.Drawing.Size(175, 24);
            this.SousFamilleComboBox.TabIndex = 24;
            // 
            // FamilleComboBox
            // 
            this.FamilleComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FamilleComboBox.FormattingEnabled = true;
            this.FamilleComboBox.Location = new System.Drawing.Point(175, 142);
            this.FamilleComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.FamilleComboBox.Name = "FamilleComboBox";
            this.FamilleComboBox.Size = new System.Drawing.Size(175, 24);
            this.FamilleComboBox.TabIndex = 23;
            this.FamilleComboBox.SelectedIndexChanged += new System.EventHandler(this.FamilleComboBox_SelectedIndexChanged);
            // 
            // MarqueComboBox
            // 
            this.MarqueComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.MarqueComboBox.FormattingEnabled = true;
            this.MarqueComboBox.Location = new System.Drawing.Point(175, 90);
            this.MarqueComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MarqueComboBox.Name = "MarqueComboBox";
            this.MarqueComboBox.Size = new System.Drawing.Size(175, 24);
            this.MarqueComboBox.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(75, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 16);
            this.label4.TabIndex = 21;
            this.label4.Text = "Famille :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(75, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 16);
            this.label3.TabIndex = 20;
            this.label3.Text = "Marque :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(75, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "Sous famille :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Référence :";
            // 
            // RefTextBox
            // 
            this.RefTextBox.Location = new System.Drawing.Point(175, 33);
            this.RefTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RefTextBox.Name = "RefTextBox";
            this.RefTextBox.Size = new System.Drawing.Size(175, 22);
            this.RefTextBox.TabIndex = 17;
            // 
            // AjoutArticleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 567);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.AddButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.QteTextBox);
            this.Controls.Add(this.DescriptionRTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.PrixTextBox);
            this.Controls.Add(this.SousFamilleComboBox);
            this.Controls.Add(this.FamilleComboBox);
            this.Controls.Add(this.MarqueComboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RefTextBox);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(441, 606);
            this.MinimumSize = new System.Drawing.Size(441, 606);
            this.Name = "AjoutArticleForm";
            this.Text = "Ajouter un article";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox QteTextBox;
        private System.Windows.Forms.RichTextBox DescriptionRTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox PrixTextBox;
        private System.Windows.Forms.ComboBox SousFamilleComboBox;
        private System.Windows.Forms.ComboBox FamilleComboBox;
        private System.Windows.Forms.ComboBox MarqueComboBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox RefTextBox;
    }
}