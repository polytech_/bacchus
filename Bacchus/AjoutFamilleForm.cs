﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class AjoutFamilleForm : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public AjoutFamilleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event de la fermeture de la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Event de l'ajout du nouvelle famille
        /// Vérifie que le champ du nom de la famille 
        /// ne soit pas vide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (NomFamilleTextBox.Text == "")
            {
                MessageBox.Show(@"Veuillez remplir le champ Nom de la famille", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                //On récupère le nom de la famille à partir de la textbox
                var Nom = NomFamilleTextBox.Text;

                //On instancie un objet Famille à partir du nom récupéré (on met la reférence à 0 car elle est gérée automatiquement dans le DAO)
                var Famille = new Famille(0, Nom);

                //Si la famille existe déjà dans la bdd on en informe l'utilisateur à travers une box
                if (((FormMain)Owner).BacchusData.GetFamille(Nom) != null)
                {
                    MessageBox.Show(@"Il existe déjà une famille associée au nom inséré, merci d'en choisir un autre", @"Attention", MessageBoxButtons.OK);
                }
                //Sinon on l'ajoute et on actualise !
                else
                {
                    DAO.AddFamille(Famille);
                    ((FormMain)Owner).BacchusData.AddFamille(Famille);
                    ((FormMain)Owner).Refresh();
                    Close();
                }
            }

        }

    }
}
