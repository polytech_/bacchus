﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IOCSV;

namespace Bacchus
{
    public partial class FormExport : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public FormExport()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Ouvre une fenetre pour sélectionner un fichier .csv à importer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChooseFileBtn_Click(object sender, EventArgs e)
        {
            var SaveFileDialog = new SaveFileDialog { Filter = @"CSV files (*.csv)|*.csv" };

            if (SaveFileDialog.ShowDialog(this) != DialogResult.OK)
            {
                return;
            }

            var FilePath = SaveFileDialog.FileName;
            FilePathInput.Text = FilePath;
        }

        /// <summary>
        /// Exporte les données
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EcrModeBtn_Click(object sender, EventArgs e)
        {
        

            if (FilePathInput.Text == "")
            {
                MessageBox.Show(@"Vous n'avez pas sélectionné de fichier", @"Attention", MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
                return;
            }

            FileOperations.ExportCsv(FilePathInput.Text, ((FormMain)Owner).BacchusData, ProgressBar);

            var Result = MessageBox.Show(@"Données Exportées", @"Information", MessageBoxButtons.OK,
                MessageBoxIcon.Information);
            if (Result == DialogResult.OK)
            {
                Close();
            }
        }

    }
}
