﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class AjoutSousFamilleForm : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public AjoutSousFamilleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event de l'ajout du nouvelle famille
        /// Vérifie que le champ du nom de la sous famille 
        /// ne soit pas vide
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (NomSousFamilleTextBox.Text == "")
            {
                MessageBox.Show(@"Veuillez remplir le champ Nom de la sous-famille", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                var BacchusModel = ((FormMain)Owner).BacchusData;
                var Nom = NomSousFamilleTextBox.Text;
                var Famille = BacchusModel.GetFamille(FamilleComboBox.Text);
                var SousFamille = new SousFamille(0, Famille, Nom);

                if (BacchusModel.GetSousFamille(Nom) != null)
                {
                    MessageBox.Show(@"Une sous famille associée à ce nom existe déjà dans la base de données, merci d'en choisir un autre", @"Attention", MessageBoxButtons.OK);
                }
                else
                {
                    DAO.AddSousFamille(SousFamille);
                    BacchusModel.AddSousFamille(SousFamille);
                    ((FormMain)Owner).Refresh();
                    Close();
                }
            }

        }

        /// <summary>
        /// Initialise les élements de la fenêtre
        /// </summary>
        public void Init()
        {

            //Remplissage de la combobox des familles avec la liste des familles
            var BacchusData = ((FormMain)Owner).BacchusData;

            FamilleComboBox.DataSource = BacchusData.FamilleList;
            FamilleComboBox.DisplayMember = "Nom";
            FamilleComboBox.ValueMember = "RefFamille";
        }

        /// <summary>
        /// Event de la fermeture de la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }

     
    }
}
