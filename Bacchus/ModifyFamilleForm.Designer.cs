﻿namespace Bacchus
{
    partial class ModifyFamilleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AbortButton = new System.Windows.Forms.Button();
            this.ConfirmButton = new System.Windows.Forms.Button();
            this.NomFamilleTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.RefFamilleTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AbortButton
            // 
            this.AbortButton.Location = new System.Drawing.Point(218, 123);
            this.AbortButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.AbortButton.Name = "AbortButton";
            this.AbortButton.Size = new System.Drawing.Size(87, 28);
            this.AbortButton.TabIndex = 24;
            this.AbortButton.Text = "Annuler";
            this.AbortButton.UseVisualStyleBackColor = true;
            this.AbortButton.Click += new System.EventHandler(this.AbortButton_Click);
            // 
            // ConfirmButton
            // 
            this.ConfirmButton.Location = new System.Drawing.Point(124, 123);
            this.ConfirmButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ConfirmButton.Name = "ConfirmButton";
            this.ConfirmButton.Size = new System.Drawing.Size(87, 28);
            this.ConfirmButton.TabIndex = 23;
            this.ConfirmButton.Text = "Confirmer";
            this.ConfirmButton.UseVisualStyleBackColor = true;
            this.ConfirmButton.Click += new System.EventHandler(this.ConfirmButton_Click);
            // 
            // NomFamilleTextBox
            // 
            this.NomFamilleTextBox.Location = new System.Drawing.Point(188, 66);
            this.NomFamilleTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.NomFamilleTextBox.Name = "NomFamilleTextBox";
            this.NomFamilleTextBox.Size = new System.Drawing.Size(203, 22);
            this.NomFamilleTextBox.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "Nom de la famille :";
            // 
            // RefFamilleTextBox
            // 
            this.RefFamilleTextBox.Enabled = false;
            this.RefFamilleTextBox.Location = new System.Drawing.Point(188, 22);
            this.RefFamilleTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.RefFamilleTextBox.Name = "RefFamilleTextBox";
            this.RefFamilleTextBox.Size = new System.Drawing.Size(203, 22);
            this.RefFamilleTextBox.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Référence de la famille :";
            // 
            // ModifyFamilleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 174);
            this.Controls.Add(this.AbortButton);
            this.Controls.Add(this.ConfirmButton);
            this.Controls.Add(this.NomFamilleTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RefFamilleTextBox);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(441, 213);
            this.MinimumSize = new System.Drawing.Size(441, 213);
            this.Name = "ModifyFamilleForm";
            this.Text = "Modifier une famille";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AbortButton;
        private System.Windows.Forms.Button ConfirmButton;
        private System.Windows.Forms.TextBox NomFamilleTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox RefFamilleTextBox;
        private System.Windows.Forms.Label label1;
    }
}