﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class ModifyFamilleForm : Form
    {
        /// <summary>
        /// Le nom de la famille
        /// </summary>
        private string NomFamille;

        /// <summary>
        /// La référence de la famille
        /// </summary>
        private int RefFamille;

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public ModifyFamilleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialise les données de la famille à modifier
        /// </summary>
        /// <param name="Famille">La famille à modifier</param>
        public void InitData(Famille Famille)
        {
            //On stock les valeurs
            NomFamille = Famille.Nom;
            RefFamille = Famille.RefFamille;

            // On remplit les champs texte
            RefFamilleTextBox.Text = Famille.RefFamille.ToString();
            NomFamilleTextBox.Text = Famille.Nom;

            
        }


        /// <summary>
        /// Event du click sur le bouton pour confirmer la modification de la famille
        /// Vérifie que les champs ne sont pas vides
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (NomFamilleTextBox.Text == "")
            {
                MessageBox.Show(@"Veuillez remplir le champ Nom de la famille", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                var FamilleExistente = ((FormMain)Owner).BacchusData.GetFamille(NomFamilleTextBox.Text);
          
                if (FamilleExistente != null && FamilleExistente.RefFamille != RefFamille)
                {
                    MessageBox.Show(@"Une famille avec ce nom existe déjà, merci d'en rentrer un qui n'existe pas dans la base de donnée", @"Attention", MessageBoxButtons.OK);
                }
                else
                {
                    var Famille = ((FormMain)Owner).BacchusData.GetFamille(RefFamille);
                    Famille.Nom = NomFamilleTextBox.Text;

                    DAO.Modify(Famille);
                    ((FormMain)Owner).UpdateListView();
                    Close();
                }
            }

        }

        /// <summary>
        /// Event de la fermeture de la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
