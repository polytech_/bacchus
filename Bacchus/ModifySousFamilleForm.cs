﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class ModifySousFamilleForm : Form
    {

        /// <summary>
        /// La nom de la sous famille
        /// </summary>
        private string NomSousFamille;

        /// <summary>
        /// La référence de la sous famille
        /// </summary>
        private int RefSousFamille;

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public ModifySousFamilleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialise les données de la sous famille à modifier
        /// </summary>
        /// <param name="SousFamille">La sous famille à modifier</param>
        public void InitData(SousFamille SousFamille)
        {

            //On stock les valeurs du paramètre en mémoire
            NomSousFamille = SousFamille.Nom;
            RefSousFamille = SousFamille.RefSousFamille;

            // On remplit les champs texte
            NomSousFamilleTextBox.Text = SousFamille.Nom;
            RefSousFamilleTextBox.Text = SousFamille.RefSousFamille.ToString();

            var BacchusData = ((FormMain)Owner).BacchusData;

            // On remplit la comboBox des familles
            FamilleComboBox.DataSource = BacchusData.FamilleList;
            FamilleComboBox.DisplayMember = "Nom";
            FamilleComboBox.ValueMember = "RefFamille";
            FamilleComboBox.SelectedItem = SousFamille.Famille;
        }

        /// <summary>
        /// Event du click sur le bouton pour confirmer la modification de la sous famille
        /// Vérifie que les champs ne sont pas vides
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ConfirmButton_Click(object sender, EventArgs e)
        {
            if (NomSousFamilleTextBox.Text == "")
            {
                MessageBox.Show(@"Veuillez remplir le champ Nom de la sous famille", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                var BacchusModel = ((FormMain)Owner).BacchusData;

                var Famille = BacchusModel.GetFamille(FamilleComboBox.Text);

                var SousFamilleExistente = BacchusModel.GetSousFamille(NomSousFamille);

                if (SousFamilleExistente != null && SousFamilleExistente.RefSousFamille != RefSousFamille)
                {
                    MessageBox.Show(@"Une sous famille avec ce nom existe déjà", @"Attention", MessageBoxButtons.OK);
                }
                else
                {
                    var SousFamille = BacchusModel.GetSousFamille(RefSousFamille);
                    SousFamille.Famille = Famille;
                    SousFamille.Nom = NomSousFamilleTextBox.Text;

                    DAO.Modify(SousFamille);
                    ((FormMain)Owner).UpdateListView();
                    Close();
                }
            }

        }

        /// <summary>
        /// Event de la fermeture de la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
