﻿using DAOService;
using Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bacchus
{
    public partial class AjoutArticleForm : Form
    {
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public AjoutArticleForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initialise les élements (combobox) de la fenêtre avec les données BacchusData
        /// </summary>
        public void Init()
        {
            var BacchusData = ((FormMain)Owner).BacchusData;

            FamilleComboBox.DataSource = BacchusData.FamilleList;
            FamilleComboBox.DisplayMember = "Nom";
            FamilleComboBox.ValueMember = "RefFamille";

            MarqueComboBox.DataSource = BacchusData.MarqueList;
            MarqueComboBox.DisplayMember = "Nom";
            MarqueComboBox.ValueMember = "RefMarque";

            var Famille = BacchusData.GetFamille((int)FamilleComboBox.SelectedValue);
            var SousFamille = DAO.GetSousFamillesByFamille(Famille);
            SousFamilleComboBox.DataSource = SousFamille;
            SousFamilleComboBox.DisplayMember = "Nom";
            SousFamilleComboBox.ValueMember = "RefSousFamille";
        }

        /// <summary>
        /// Charge la liste des sous famille de la famille selectionnée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FamilleComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Récupération des données importées
            var BacchusData = ((FormMain)Owner).BacchusData;

            int RefFamille = 0;

            //Booléen indiquant si le parsage de la réference de la famille et fournissant le résultat dans la variable RefFamille
            bool parseOK = Int32.TryParse(FamilleComboBox.SelectedValue.ToString(), out RefFamille);

            //On récupère la famille
            var Famille = BacchusData.GetFamille(RefFamille);

            //On récupère la liste des sous familles correspondant à la famille sélectionnée
            var SousFamilles = DAO.GetSousFamillesByFamille(Famille);
            
            //On affecte les données récupérées à la combobox
            SousFamilleComboBox.DataSource = SousFamilles;
            SousFamilleComboBox.DisplayMember = "Nom";
            SousFamilleComboBox.ValueMember = "RefSousFamille";
        }

        /// <summary>
        /// N'accepte que des entiers et des points dans la textBox Prix (ex. 19.99)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PrixTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && (((TextBox)sender).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// N'accepte que des entiers dans la textBox Quantité (ex. 19)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QteTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
                (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Event du click sur le bouton pour annuler l'ajout
        /// Ferme la fenetre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AbortButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Event du click sur le bouton pour ajouter l'article
        /// Vérifie que les champs ne sont pas vides
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddButton_Click(object sender, EventArgs e)
        {
            //On récupère les données stockées précédemment
            var BacchusData = ((FormMain)Owner).BacchusData;

            //Si l'utilisateur ne rentre aucune valeur de quantité, de prix et de description, on affiche une box l'informant qu'il n'a rien rempli
            if (QteTextBox.Text.Equals("") || PrixTextBox.Text.Equals("") || RefTextBox.Text.Equals("") ||
                DescriptionRTextBox.Text.Equals(""))
            {
                MessageBox.Show(@"Vous n'avez pas remplit tous les champs concernant l'article, merci de tout remplir", @"Attention", MessageBoxButtons.OK);
            }
            else
            {
                //Si l'utilisateur rentre la réference d'un article déjà existant, on affiche une box l'informant de cela
                if (BacchusData.GetArticle(RefTextBox.Text) != null)
                {
                    MessageBox.Show(@"Il y a déjà un article associé à cette référence, merci de réessayer", @"Attention", MessageBoxButtons.OK);
                }
                else
                {
                    //Si l'utilisateur rentre la réference d'un article ne correspondant pas au format déjà présent 
                    //(format du fichier fournit par Mr Dagnas : Données à intégrer.csv) on affiche une box l'informant de cela
                    if (RefTextBox.Text.IndexOf('F') == -1 || RefTextBox.Text.Length != 8)
                    {
                        MessageBox.Show(
                            @"La référence de l'article doit contenir un F en première position et 8 caractères au total",
                            @"Attention", MessageBoxButtons.OK);
                    }
                    //Si toutes les conditions sont réunies pour ajouter l'article, on l'ajoute !
                    else
                    {

                        //On commence par récupérer les données
                        var SousFamille = BacchusData.GetSousFamille(SousFamilleComboBox.Text);
                        var Marque = BacchusData.GetMarque(MarqueComboBox.Text);
                        var Description = DescriptionRTextBox.Text;
                        var Prix = PrixTextBox.Text;
                        var Quantite = QteTextBox.Text;
                        var Reference = RefTextBox.Text;

                        var PrixHt = float.Parse(Prix, CultureInfo.InvariantCulture);
                        var QuantiteInt = int.Parse(Quantite);

                        //On crée une instance d'Article à partir des données récupérées
                        var Article = new Article(Reference, Description, SousFamille, Marque, PrixHt, QuantiteInt);

                        //Enfin, on ajoute l'article à notre classe d'historique BacchusData et à notre base de données
                        BacchusData.AddArticle(Article);
                        DAO.AddArticle(Article);

                        //On actualise l'affichage ainsi que le nombre d'articles dans le StatusStrip
                        ((FormMain)Owner).UpdateListView();
                        ((FormMain)Owner).UpdateNbArticles(BacchusData.ArticleList.Count);
                        Close();
                    }
                }
            }
        }
    }
}
